export function fetchPhotosFromApi({number}, country = 'de') {
  const headers = new Headers();
  headers.append('Authorization', 'Bearer 73fe2b2915f41e6bbaed6b84cfd20fc6');
  return fetch(
    `https://api.deutschebahn.com/bahnhofsfotos/v1/${country}/stations/${number}`,
    {headers}
  ).then(response => response.json());
}

export function fetchStations({city}) {
  const headers = new Headers();
  headers.append('Authorization', 'Bearer 568143b0a10b6f942431889ea9ad728e');
  return fetch(
    `https://api.deutschebahn.com/stada/v2/stations?searchstring=*${city}*`,
    {headers}
  ).then(response => response.json()).catch(response => console.log(response));
}
