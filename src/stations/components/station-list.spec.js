import chai from 'chai';
import chaiDom from 'chai-dom';
import createStationList from './station-list';

chai.use(chaiDom);
chai.should();

describe('Component: StationList', () => {
  it('should update stations correctly', () => {
    const node = document.createElement('node');
    const renderStations = createStationList(node, station => station);
    renderStations([{name: 'Bahnhof 1'}, {name: '2'}, {name: '3'}, {name: '4'}]);
    node.querySelector('ul').should.have.length(4);
    node.should.contain.text('Bahnhof 1');
    renderStations([{name: 'Bahnhof'}]);
    node.querySelector('ul').should.have.length(1);
    node.should.have.text('Bahnhof');
  });

  it('should update stations correctly', (done) => {
    const node = document.createElement('node');
    const callback = station => { station.name.should.be.equal('Bahnhof 1'); done(); };
    const renderStations = createStationList(node, callback);
    renderStations([{name: 'Bahnhof 1'}, {name: '2'}, {name: '3'}, {name: '4'}]);

    node.querySelector('ul').firstChild.querySelector('span').click();

  });
});

