import chai from 'chai';
import chaiDom from 'chai-dom';
import StationDetail from './station-detail';

chai.use(chaiDom);
chai.should();

describe('Class: StationDetail', () => {
  it('should display all necessary information on Ulm Hbf', () => {
    const node = document.createElement('node');
    const stationDetail = new StationDetail(node);
    stationDetail.station = {name: 'Ulm Hbf', hasParking: true, hasWiFi: true};
    node.should.contain.text('Ulm Hbf');
    node.should.contain.text('WiFi verfügbar: true');
    node.should.contain.text('Parkplätze verfügbar: true');
  });
});
