import chai from 'chai';
import chaiDom from 'chai-dom';
import StationServices from './station-services';

chai.use(chaiDom);
chai.should();

describe('Station Services', () => {
  it('should run karma tests', () => {
    const node = document.createElement('div');
    // node.textContent = 'Hallo, ich bin ein HTML Knoten!';
    const stationService = new StationServices(node);
    stationService._services = {hasWiFi: true, hasParking: false};
    stationService.render();
    node.should.contain.text('Wifi verfügbar: true');
    node.should.contain.text('Parkplätze verfügbar: false');
  });
});
