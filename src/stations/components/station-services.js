import {AbstractView} from './../../common';

/**
function StationServices(node) {
  this._services = {
    hasWiFi: true,
    hasParking: null
  };
  this.node = node;

  this.element = () => {
    const div = document.createElement('div');
    div.textContent = `Wifi verfügbar: ${this._services.hasWiFi}, Parkplätze verfügbar: ${this._services.hasParking} `;
    return div;
  };

}
 */
// StationServices.prototype = new AbstractView();

class StationServices extends AbstractView {
  constructor(node) {
    super();
    this.node = node;
  }

  set services(newVal) {
    this._services = newVal;
    this.render();
  }

  element() {
    const div = document.createElement('div');
    div.textContent = `Wifi verfügbar: ${this._services.hasWiFi}, Parkplätze verfügbar: ${this._services.hasParking} `;
    return div;
  }
}

export default StationServices;
