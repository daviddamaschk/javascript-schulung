import {AbstractView} from './../../common';
import {fetchStations} from './../remote/station-remotecalls';
import createStationList from './station-list';

export default class StationSearch extends AbstractView {

  constructor(node, searchAction) {
    super();
    this.node = node;
    this.searchAction = searchAction;
    this.search();
  }

  search() {
    this.render();
  }

  element() {
    const input = document.createElement('input');
    input.type = 'text';
    input.addEventListener('blur', () => this.searchAction(input.value));
    return input;
  }
}