import {AbstractView} from './../../common';

class StationDetail extends AbstractView {
  constructor(node) {
    super();
    this.node = node;
  }

  set station(newVal) {
    this._services = newVal;
    this.render();
  }

  element() {
    const div = document.createElement('div');
    div.textContent = `${this._services.name} WiFi verfügbar: ${this._services.hasWiFi}, 
    Parkplätze verfügbar: ${this._services.hasParking} `;
    return div;
  }
}

export default StationDetail;
