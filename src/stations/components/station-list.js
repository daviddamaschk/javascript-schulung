import {renderOnto, AbstractView} from './../../common';

class StationListEntry extends AbstractView {

  constructor(node, onClick) {
    super();
    this.node = node;
    this._station = {};
    this.onClick = onClick;
  }

  element() {
    const span = document.createElement('span');
    span.addEventListener('click', () => this.onClick(this._station));
    span.textContent = this._station.name;
    return span;
  }

  set station(station) {
    this._station = station;
    this.render();
  }

}

const createStationList = (node, onClick) => stations => {
  const div = document.createElement('div');
  const ul = document.createElement('ul');

  stations.forEach(
    a => {
      const li = document.createElement('li');
      const entry = new StationListEntry(li, onClick);
      entry.station = a;
      ul.appendChild(li);
    }
  );
  div.appendChild(ul);
  renderOnto(node, div);
};

export default createStationList;