import StationServices from './stations/components/station-services';
import StationDetail from './stations/components/station-detail';
import createStationList from './stations/components/station-list';
import './index.scss';
import {stationsDarmstadt} from './test/station-samples';
import {renderOnto} from './common';
import {fetchStations} from './stations/remote/station-remotecalls';
import StationSearch from './stations/components/station-search';

const stationDetail = new StationDetail(document.getElementById('station-detail'));
const renderStationList = createStationList(document.getElementById('station-list'), station => {
  stationDetail.station = station;
});

/**
 window.StationServices = StationServices;
 window.StationDetail = StationDetail;
 window.createStationList = createStationList;
 */
// window.renderStations = renderStations;
// renderStationList(stationsDarmstadt);

function reRenderList(createList) {
  const renderStations = createList(document.body, () => {
  });
  window.setInterval(() => {
    renderStations(lotsOfStations); // eslint-disable-line no-undef
  }, 500);
}

function isSmaller(index, ul) {
  return index < ul.children.length;
}

function mapStations(ul, onSelect) {
  let onClick = () => {
  };
  return function update(stations) {
    onClick = (index) => onSelect(stations[index]);
    stations.forEach((station, index) => {
      if (isSmaller(index, ul)) {
        ul.children[index].textContent = station.name;
      } else {
        const li = document.createElement('li');
        li.addEventListener('click', () => {
          onClick(index);
        });
        li.textContent = station.name;
        ul.appendChild(li);
      }
    });
    while (ul.children.length > stations.length) {
      ul.removeChild(ul.lastChild);
    }
  };
}

function createFastList(node, onSelect) {
  const ul = document.createElement('ul');
  renderOnto(node, ul);
  return mapStations(ul, onSelect);
}

const searchAction = (value) => {
  fetchStations({city: value}).then(
    (response) => {
      const div = document.getElementById('station-list');
      const showList = createStationList(div, () => {
      });

      if (response.result !== undefined) {
        showList(response.result);
      } else {
        showList([]);
      }
    }
  );
};

window.createStationList = createStationList;
window.createFastList = createFastList;
window.reRenderList = reRenderList;
window.fetchStations = fetchStations;
window.StationSearch = StationSearch;

const div = document.getElementById('station-input');
const StationSearchInputField = new StationSearch(div, searchAction);
// reRenderList(createFastList)