export function renderOnto(node, element) {
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
  node.appendChild(element);
}

export function AbstractView() {
  this.render = function render() {
    renderOnto(this.node, this.element());
  };
}
