export const stationsUlm = [
  {
    number: 5734,
    name: 'Ulm-Donautal',
    mailingAddress: {
      city: 'Ulm',
      zipcode: '89079',
      street: 'Nicolaus-Otto-Str.'
    },
    category: 6,
    priceCategory: 6,
    hasParking: false,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'yes',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Baden-Württemberg',
    regionalbereich: {
      number: 6,
      name: 'RB Südwest',
      shortName: 'RB SW'
    },
    aufgabentraeger: {
      shortName: 'NVBW',
      name: 'Nahverkehrsgesellschaft Baden-Württemberg mbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.BadenWuerttemberg@deutschebahn.com',
      name: 'Bahnhofsmanagement Ulm'
    },
    szentrale: {
      number: 35,
      publicPhoneNumber: '0731/1021055',
      name: 'Ulm Hbf'
    },
    stationManagement: {
      number: 214,
      name: 'Ulm'
    },
    evaNumbers: [
      {
        number: 8005954,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.940355,
            48.36634
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'TUDT',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.940333693,
            48.366359955
          ]
        }
      }
    ]
  },
  {
    number: 6323,
    name: 'Ulm Hbf',
    mailingAddress: {
      city: 'Ulm',
      zipcode: '89073',
      street: 'Bahnhofplatz 1'
    },
    category: 2,
    priceCategory: 2,
    hasParking: true,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: true,
    hasLockerSystem: true,
    hasTaxiRank: true,
    hasTravelNecessities: true,
    hasSteplessAccess: 'partial',
    hasMobilityService: 'Ja, um Voranmeldung unter 01806 512 512 wird gebeten',
    hasWiFi: true,
    hasTravelCenter: true,
    hasRailwayMission: true,
    hasDBLounge: false,
    hasLostAndFound: true,
    hasCarRental: true,
    federalState: 'Baden-Württemberg',
    regionalbereich: {
      number: 6,
      name: 'RB Südwest',
      shortName: 'RB SW'
    },
    aufgabentraeger: {
      shortName: 'NVBW',
      name: 'Nahverkehrsgesellschaft Baden-Württemberg mbH'
    },
    DBinformation: {
      availability: {
        monday: {
          fromTime: '06:00',
          toTime: '24:00'
        },
        tuesday: {
          fromTime: '06:00',
          toTime: '24:00'
        },
        wednesday: {
          fromTime: '06:00',
          toTime: '24:00'
        },
        thursday: {
          fromTime: '06:00',
          toTime: '24:00'
        },
        friday: {
          fromTime: '06:00',
          toTime: '24:00'
        },
        saturday: {
          fromTime: '06:00',
          toTime: '24:00'
        },
        sunday: {
          fromTime: '06:00',
          toTime: '24:00'
        }
      }
    },
    localServiceStaff: {
      availability: {
        monday: {
          fromTime: '06:30',
          toTime: '23:15'
        },
        tuesday: {
          fromTime: '06:30',
          toTime: '23:15'
        },
        wednesday: {
          fromTime: '06:30',
          toTime: '23:15'
        },
        thursday: {
          fromTime: '06:30',
          toTime: '23:15'
        },
        friday: {
          fromTime: '06:30',
          toTime: '23:15'
        },
        saturday: {
          fromTime: '06:30',
          toTime: '23:15'
        },
        sunday: {
          fromTime: '06:30',
          toTime: '23:15'
        },
        holiday: {
          fromTime: '06:30',
          toTime: '23:15'
        }
      }
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.BadenWuerttemberg@deutschebahn.com',
      name: 'Bahnhofsmanagement Ulm'
    },
    szentrale: {
      number: 35,
      publicPhoneNumber: '0731/1021055',
      name: 'Ulm Hbf'
    },
    stationManagement: {
      number: 214,
      name: 'Ulm'
    },
    evaNumbers: [
      {
        number: 8000170,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.982227,
            48.399437
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'TU',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.982813107,
            48.399645163
          ]
        }
      }
    ]
  },
  {
    number: 6324,
    name: 'Ulm Ost',
    mailingAddress: {
      city: 'Ulm',
      zipcode: '89073',
      street: 'An der Brenzbahn'
    },
    category: 6,
    priceCategory: 6,
    hasParking: true,
    hasBicycleParking: false,
    hasLocalPublicTransport: false,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'yes',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Baden-Württemberg',
    regionalbereich: {
      number: 6,
      name: 'RB Südwest',
      shortName: 'RB SW'
    },
    aufgabentraeger: {
      shortName: 'NVBW',
      name: 'Nahverkehrsgesellschaft Baden-Württemberg mbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.BadenWuerttemberg@deutschebahn.com',
      name: 'Bahnhofsmanagement Ulm'
    },
    szentrale: {
      number: 35,
      publicPhoneNumber: '0731/1021055',
      name: 'Ulm Hbf'
    },
    stationManagement: {
      number: 214,
      name: 'Ulm'
    },
    evaNumbers: [
      {
        number: 8005952,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.994631,
            48.407159
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'TUO',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.994626571,
            48.407160767
          ]
        }
      }
    ]
  },
  {
    number: 6325,
    name: 'Ulm-Söflingen',
    mailingAddress: {
      city: 'Ulm',
      zipcode: '89075',
      street: 'In der Wanne 1'
    },
    category: 6,
    priceCategory: 6,
    hasParking: false,
    hasBicycleParking: false,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'partial',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Baden-Württemberg',
    regionalbereich: {
      number: 6,
      name: 'RB Südwest',
      shortName: 'RB SW'
    },
    aufgabentraeger: {
      shortName: 'NVBW',
      name: 'Nahverkehrsgesellschaft Baden-Württemberg mbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.BadenWuerttemberg@deutschebahn.com',
      name: 'Bahnhofsmanagement Ulm'
    },
    szentrale: {
      number: 35,
      publicPhoneNumber: '0731/1021055',
      name: 'Ulm Hbf'
    },
    stationManagement: {
      number: 214,
      name: 'Ulm'
    },
    evaNumbers: [
      {
        number: 8005955,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.958849,
            48.402726
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'TU  F',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            9.958842219,
            48.40270807
          ]
        }
      }
    ]
  }
];

export const stationsDarmstadt = [
  {
    number: 1126,
    name: 'Darmstadt Hbf',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64293',
      street: 'Am Hauptbahnhof 20'
    },
    category: 2,
    priceCategory: 2,
    hasParking: true,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: true,
    hasLockerSystem: true,
    hasTaxiRank: true,
    hasTravelNecessities: true,
    hasSteplessAccess: 'yes',
    hasMobilityService: 'Ja, um Voranmeldung unter 01806 512 512 wird gebeten',
    hasWiFi: true,
    hasTravelCenter: true,
    hasRailwayMission: true,
    hasDBLounge: false,
    hasLostAndFound: true,
    hasCarRental: true,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    DBinformation: {
      availability: {
        monday: {
          fromTime: '06:00',
          toTime: '22:30'
        },
        tuesday: {
          fromTime: '06:00',
          toTime: '22:30'
        },
        wednesday: {
          fromTime: '06:00',
          toTime: '22:30'
        },
        thursday: {
          fromTime: '06:00',
          toTime: '22:30'
        },
        friday: {
          fromTime: '06:00',
          toTime: '22:30'
        },
        saturday: {
          fromTime: '08:45',
          toTime: '19:30'
        },
        sunday: {
          fromTime: '08:45',
          toTime: '20:55'
        },
        holiday: {
          fromTime: '08:45',
          toTime: '20:55'
        }
      }
    },
    localServiceStaff: {
      availability: {
        monday: {
          fromTime: '06:15',
          toTime: '22:15'
        },
        tuesday: {
          fromTime: '06:15',
          toTime: '22:15'
        },
        wednesday: {
          fromTime: '06:15',
          toTime: '22:15'
        },
        thursday: {
          fromTime: '06:15',
          toTime: '22:15'
        },
        friday: {
          fromTime: '06:15',
          toTime: '22:15'
        },
        saturday: {
          fromTime: '09:00',
          toTime: '19:15'
        },
        sunday: {
          fromTime: '09:00',
          toTime: '20:30'
        },
        holiday: {
          fromTime: '09:00',
          toTime: '20:30'
        }
      }
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Darmstadt'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 157,
      name: 'Darmstadt'
    },
    evaNumbers: [
      {
        number: 8000068,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.629636,
            49.872503
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FD',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.629857406,
            49.872781809
          ]
        }
      }
    ]
  },
  {
    number: 1127,
    name: 'Darmstadt Nord',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64293',
      street: 'Frankfurter Str. 127'
    },
    category: 5,
    priceCategory: 5,
    hasParking: false,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: true,
    hasSteplessAccess: 'no',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Darmstadt'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 157,
      name: 'Darmstadt'
    },
    evaNumbers: [
      {
        number: 8001375,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.654311,
            49.892136
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FDN',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.653748556,
            49.892113245
          ]
        }
      }
    ]
  },
  {
    number: 1128,
    name: 'Darmstadt Ost',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64287',
      street: 'Erbacherstr. 87'
    },
    category: 6,
    priceCategory: 6,
    hasParking: true,
    hasBicycleParking: false,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'yes',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Darmstadt'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 157,
      name: 'Darmstadt'
    },
    evaNumbers: [
      {
        number: 8001376,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.673867,
            49.874743
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FDO',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.673755144,
            49.874902797
          ]
        }
      }
    ]
  },
  {
    number: 1129,
    name: 'Darmstadt Süd',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64299',
      street: 'Haardtring 275'
    },
    category: 5,
    priceCategory: 5,
    hasParking: false,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'no',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Darmstadt'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 157,
      name: 'Darmstadt'
    },
    evaNumbers: [
      {
        number: 8001377,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.636586,
            49.855426
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FDS',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.637119741,
            49.85415706
          ]
        }
      }
    ]
  },
  {
    number: 1130,
    name: 'Darmstadt-Arheilgen',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64291',
      street: 'Weiterstädter-Str. 1'
    },
    category: 6,
    priceCategory: 6,
    hasParking: true,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'yes',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Frankfurt(M)'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 161,
      name: 'Frankfurt a. M.'
    },
    evaNumbers: [
      {
        number: 8001378,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.645343,
            49.913014
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FDA',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.645350484,
            49.913011994
          ]
        }
      }
    ]
  },
  {
    number: 1131,
    name: 'Darmstadt-Eberstadt',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64297',
      street: 'An der Eisenbahn 1'
    },
    category: 4,
    priceCategory: 4,
    hasParking: false,
    hasBicycleParking: false,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: true,
    hasTravelNecessities: false,
    hasSteplessAccess: 'no',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Darmstadt'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 157,
      name: 'Darmstadt'
    },
    evaNumbers: [
      {
        number: 8001379,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.62582,
            49.813922
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FDE',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.626039162,
            49.814436537
          ]
        }
      }
    ]
  },
  {
    number: 1132,
    name: 'Darmstadt-Kranichstein',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64289',
      street: 'Parkstraße 11'
    },
    category: 6,
    priceCategory: 6,
    hasParking: true,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'partial',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Darmstadt'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 157,
      name: 'Darmstadt'
    },
    evaNumbers: [
      {
        number: 8001380,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.679359,
            49.906826
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FDK',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.679355872,
            49.906780216
          ]
        }
      }
    ]
  },
  {
    number: 6843,
    name: 'Darmstadt-Wixhausen',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64291',
      street: 'Bahnhofstr. 2'
    },
    category: 6,
    priceCategory: 6,
    hasParking: true,
    hasBicycleParking: true,
    hasLocalPublicTransport: true,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'yes',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Frankfurt(M)'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 161,
      name: 'Frankfurt a. M.'
    },
    evaNumbers: [
      {
        number: 8006528,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.647751,
            49.930248
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FWX',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.647895438,
            49.930152513
          ]
        }
      }
    ]
  },
  {
    number: 7161,
    name: 'Darmstadt-Lichtwiese',
    mailingAddress: {
      city: 'Darmstadt',
      zipcode: '64287',
      street: 'Petersenstraße 24'
    },
    category: 6,
    priceCategory: 6,
    hasParking: false,
    hasBicycleParking: false,
    hasLocalPublicTransport: false,
    hasPublicFacilities: false,
    hasLockerSystem: false,
    hasTaxiRank: false,
    hasTravelNecessities: false,
    hasSteplessAccess: 'yes',
    hasMobilityService: 'no',
    hasWiFi: false,
    hasTravelCenter: false,
    hasRailwayMission: false,
    hasDBLounge: false,
    hasLostAndFound: false,
    hasCarRental: false,
    federalState: 'Hessen',
    regionalbereich: {
      number: 5,
      name: 'RB Mitte',
      shortName: 'RB M'
    },
    aufgabentraeger: {
      shortName: 'RMV',
      name: 'Rhein-Main-Verkehrsverbund GmbH'
    },
    timeTableOffice: {
      email: 'DBS.Fahrplan.Hessen@deutschebahn.com',
      name: 'Bahnhofsmanagement Darmstadt'
    },
    szentrale: {
      number: 45,
      publicPhoneNumber: '069/2651055',
      name: 'Frankfurt (Main) Hbf'
    },
    stationManagement: {
      number: 157,
      name: 'Darmstadt'
    },
    evaNumbers: [
      {
        number: 8001386,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.687527,
            49.861045
          ]
        },
        isMain: true
      }
    ],
    ril100Identifiers: [
      {
        rilIdentifier: 'FDAL',
        isMain: true,
        hasSteamPermission: true,
        geographicCoordinates: {
          type: 'Point',
          coordinates: [
            8.687609574,
            49.860879265
          ]
        }
      }
    ]
  }
];
