module.exports = (config) => {
  config.set({
    frameworks: ['mocha', 'chai'],
    files: ['src/**/*.spec.js'], // files to execute
    preprocessors: {
      'src/**/*.spec.js': ['webpack'] // process test files w/ webpack
    },
    webpack: require('./webpack.config.js'), // eslint-disable-line
    reporters: ['progress'],
    port: 9876, // karma web server port
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: ['ChromeHeadless'],
    autoWatch: true // reruns tests automatically after changes
  });
};
